package ooss;

import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;
//    private List<Student> studentList;
    private Student leader;
    private Person attachPerson;

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Student getLeader() {
        return leader;
    }

    public void setLeader(Student leader) {
        this.leader = leader;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    public void assignLeader(Student student){
        if(equals(student.getKlass())){
            this.leader = student;
            String personType = this.attachPerson.getClass().getSimpleName().toLowerCase();
            System.out.println(String.format("I am %s, %s of Class %d. I know %s become Leader.",
                    this.attachPerson.getName(),personType,getNumber(),getLeader().getName()));
        }else{
            System.out.println("It is not one of us.");
        }

    }

    public boolean isLeader(Student student){
        if(student.equals(this.leader)){
            return true;
        }else{
            return false;
        }
    }

    public void attach(Person person){

        this.attachPerson = person;
    }

}
