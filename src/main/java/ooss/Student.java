package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }
    public String introduce(){
        String introduceInfo = super.introduce() + " I am a student.";
        if(this.klass != null){
            if(equals(this.klass.getLeader())){
                introduceInfo += " I am the leader of class " + this.klass.getNumber() + ".";
            }else {
                introduceInfo += " I am in class " + this.klass.getNumber() + ".";
            }
        }
        return introduceInfo;
    }
    public void join (Klass klass){
        this.klass = klass;
    }
    public boolean isIn(Klass klass){
        if(klass.equals(this.klass)){
            return true;
        }else {
            return false;
        }
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }

}
