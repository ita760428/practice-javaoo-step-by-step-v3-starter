package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private List<Klass> klassList = new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce(){
//        this.klassList.forEach();
        String introduceInfo = super.introduce() + " I am a teacher.";
        int length = this.klassList.size();
        if(this.klassList.size() != 0){
            introduceInfo += " I teach Class ";
        }
        for(Klass klass_ : klassList){
            if(this.klassList.get(length-1).equals(klass_)){
                introduceInfo += klass_.getNumber() + ".";
            }else{
                introduceInfo += klass_.getNumber() + ", ";
            }

        }
        return introduceInfo;

    }
    public boolean isHaveLeader(Klass klass){
        if(klass.getLeader() != null){
            return true;
        }else{
            return false;
        }
    }

    public void assignTo(Klass klass){
        if(this.klassList.contains(klass)){
            return;
        }
        this.klassList.add(klass);
    }

    public boolean belongsTo(Klass klass){
        for(Klass klass_ : klassList){
            if(klass_.getNumber() == klass.getNumber()){
                return true;
            }
        }
        return false;
    }

    public boolean isTeaching (Student student){
        if(this.klassList.contains(student.getKlass())){
            return true;
        }else{
            return false;
        }
    }
}
